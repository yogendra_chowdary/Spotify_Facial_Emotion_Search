import React from 'react';
import './Footer.css';
import insta from '../images/instagram.png';
import linkedin from '../images/linkedin.png';
import github from '../images/github.png';

const Footer = () => {
  return (
    <footer className="footer-container">
      <a href="https://www.instagram.com/krishnainfinite/" className="icon-link">
        <img src={insta} alt="Instagram" className="icon" />
      </a>
      <a href="https://github.com/yogendrachowdary" className="icon-link">
        <img src={github} alt="GitHub" className="icon" />
      </a>
      <a href="https://www.linkedin.com/in/yogendra-chowdary-ab37a4248/" className="icon-link">
        <img src={linkedin} alt="LinkedIn" className="icon" />
      </a>
      
    </footer>
  );
};

export default Footer;
