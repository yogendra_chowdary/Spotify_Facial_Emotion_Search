import React, { useState, useRef } from "react";
import { CloudinaryContext, Image } from "cloudinary-react";
import Webcam from "react-webcam";
import AWS from "aws-sdk";
import { useDropzone } from "react-dropzone";
import "./ImageUpload.css";
import {  toast } from "react-toastify";
import Loader from "./Loader";
import axios from "axios"; // Import Axios
import Footer from "./Footer";
import { Fade } from "react-awesome-reveal";

const cloudinaryConfig = {
  cloudName: process.env.REACT_APP_Cloud_Name,
  apiKey: process.env.REACT_APP_Api_Key,
  apiSecret: process.env.REACT_APP_Api_Secret,
};

AWS.config.update({
  accessKeyId: process.env.REACT_APP_Access_Key_Id,
  secretAccessKey: process.env.REACT_APP_Secret_Access_Key,
  region: process.env.REACT_APP_region,
});

const rekognition = new AWS.Rekognition();

const CloudinaryUpload = async (file) => {
  try {
    if (file.type === "image/jpeg" || file.type === "image/png") {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("upload_preset", "recognition");

      const response = await fetch(
        `https://api.cloudinary.com/v1_1/${cloudinaryConfig.cloudName}/image/upload`,
        {
          method: "POST",
          body: formData,
        }
      );

      if (!response.ok) {
        throw new Error("Failed to upload image to Cloudinary");
      }
      const data = await response.json();
      toast.success("Woo-hoo! Pixel power activated!💥", { 
        autoClose: 1500, // 1.2 seconds
        position: "top-center",
        theme: "dark",
      }); 
      return data.secure_url;
    } else {

      toast.error("Wrong file type", { 
        autoClose: 1500, // 1.2 seconds
        position: "top-center",
        theme: "dark",
      }); 
    }
  } catch (error) {
    console.error("Error uploading image:", error);
    throw error;
  }
};

export default function ImageUpload() {
  const [songRecommendations, setSongRecommendations] = useState([]);
  const [uploadedImage, setUploadedImage] = useState("");
  const [details, setDetails] = useState();
  const webcamRef = useRef(null);
  const suggestSongsBasedOnMood = async (mood) => {
    try {
      const response = await axios.get(
        "https://spotify23.p.rapidapi.com/search/",
        {
          params: {
            q: mood,
            type: "multi",
            offset: "0",
            limit: "5",
            numberOfTopResults: "5",
          },
          headers: {
            "X-RapidAPI-Key":
              "e561b8e150msh6e75c2479cd3037p132b74jsn07532b1603cc",
            "X-RapidAPI-Host": "spotify23.p.rapidapi.com",
          },
        }
      );
      console.log(response.data); // Print song recommendations to console
      const items = response.data.tracks.items;

      setSongRecommendations(items);
      for (var i = 0; i < items.length; i++) {
        console.log(items[i].data);
      }

      for (var x = 0; x < items.length; x++) {
        console.log(
          items[x].data.albumOfTrack.name +
            " " +
            items[x].data.albumOfTrack.sharingInfo.shareUrl
        );
        console.log(items[x].data.albumOfTrack.coverArt.sources[0].url);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: async (acceptedFiles) => {
      try {
        const uploadedUrl = await CloudinaryUpload(acceptedFiles[0]);
        setUploadedImage(uploadedUrl);
      } catch (error) {
        console.error("Error uploading dropped image:", error);
        toast.error("Error capturing/uploading image", { 
          autoClose: 1500, // 1.2 seconds
          position: "top-center",
          theme: "dark",
        }); 
      }
    },
  });

  const handleCaptureAndUpload = async () => {
    try {
      const imageSrc = webcamRef.current.getScreenshot();
      const blob = await fetch(imageSrc).then((res) => res.blob());
      const uploadedUrl = await CloudinaryUpload(blob);
      setUploadedImage(uploadedUrl);
    } catch (error) {
      console.error("Error capturing and uploading image:", error);
      toast.error("Error capturing/uploading image", { 
        autoClose: 1500, // 1.2 seconds
        position: "top-center",
        theme: "dark",
      });     }
  };

  const stopCamera = () => {
    const videoTrack = webcamRef.current.video.srcObject.getVideoTracks()[0];
    videoTrack.stop();
    toast.success("Camera Stopped!", { 
      autoClose: 1500, // 1.2 seconds
      position: "top-center",
      theme: "dark",
    });   };

  const startCamera = async () => {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({ video: true });
      webcamRef.current.video.srcObject = stream;
    } catch (error) {
      console.error("Error starting the camera:", error);
      toast.error("Error starting the camera", { 
        autoClose: 1500, // 1.2 seconds
        position: "top-center",
        theme: "dark",
      });     }
  };

  const captureScreenshot = () => {
    handleCaptureAndUpload();
  };

  const getImageBytes = async (imageUrl) => {
    const response = await fetch(imageUrl);
    const arrayBuffer = await response.arrayBuffer();
    return new Uint8Array(arrayBuffer);
  };

  async function facialAnalysis() {
    try {
      setDetails(null);
      const rekognitionParams = {
        Image: {
          Bytes: await getImageBytes(uploadedImage),
        },
        Attributes: ["ALL"],
      };

      console.log("Sending To Rekog");
      const data = await rekognition.detectFaces(rekognitionParams).promise();

      // Extract desired attributes from the detected faces
      const faceDetails = data.FaceDetails.map((faceDetail) => {
        let maxConfidenceEmotion = { type: "", confidence: 0 };

        faceDetail.Emotions.forEach((emotion) => {
          if (emotion.Confidence > maxConfidenceEmotion.confidence) {
            maxConfidenceEmotion.type = emotion.Type;
            maxConfidenceEmotion.confidence = emotion.Confidence;
          }
        });

        return {
          emotions: maxConfidenceEmotion,
          glasses: faceDetail.Eyeglasses.Value,
          sunglasses: faceDetail.Sunglasses.Value,
          gender: faceDetail.Gender.Value,
          smile: faceDetail.Smile.Value,
          ageRange: {
            low: faceDetail.AgeRange.Low,
            high: faceDetail.AgeRange.High,
          },
        };
      });

      console.log("Face details:", faceDetails);
      const att = faceDetails[0];
      setDetails(att);
      suggestSongsBasedOnMood(att.emotions.type.toLowerCase());
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <CloudinaryContext {...cloudinaryConfig}>
    <h1 className="app-heading">EmoTunes: Mood-Based Music Companion, Powered by AWS <i className="fab fa-aws"></i></h1>
   <center> <h6>
     1. Simply tap 'Capture Image', and then 'Get Facial Analysis' to proceed.
    </h6></center>
    <center> <h6>
    2. Experience facial analysis and enjoy Spotify tunes <i className="fab fa-spotify"></i> tailored to your emotions! 😁🎶
    </h6></center>  <center> <h6>
   3. If the camera below this text isn't visible, please click 'Start My Camera' and grant permission😊
    </h6></center>

      <div className="container imageupload">
     <Fade>
        <div className="webcam-container">
       
   
        <div className="webCam">
          <Webcam
            ref={webcamRef}
            screenshotFormat="image/png"
            mirrored={true}
            className="webcam"
          />

          </div>
          <div className="mt-3">
          
            <table>

              <tr>
                <td>
                  <button className="cus" onClick={captureScreenshot}>
                    <span className="text">Capture Image</span>
                  </button>
                </td>
                <td>
                <button
                    className="cus"
                    data-bs-toggle="modal"
                    data-bs-target="#staticBackdrop"
                    onClick={facialAnalysis}
                  >
                    <span className="text">Get Facial Analysis</span>
                  </button>
                 
                </td>
              </tr>
              <tr>
                <td>
                <button className="cus" onClick={startCamera}>
                    <span className="text">Start My Camera</span>
                  </button>
                </td>
                <td>
                
                  <button className="cus" onClick={stopCamera}>
                    <span className="text">Stop My Camera</span>
                  </button>
                </td>
              </tr>
            </table>
          </div>
        </div>
        </Fade>
        <div className="image-container">
          {uploadedImage && (
            <div className="image-details">
              <div className="uploaded-image">
                <Image
                  publicId={uploadedImage}
                  className="uploadedimagecontainer"
                />
              </div>
            </div>
          )}
        </div>
        <div className="container imageupload">
  {/* Your other components */}
  <div className="dropzone-container">
    <div
      {...getRootProps()}
      className="dropzone"
      style={{
        backgroundColor: "hsl(223, 63%, 37%)",
                color: "#000", // Set text color to black
      }}
    >
      <input {...getInputProps()} />
      <i className="fa-regular fa-download"></i>
      <p>Drag 'n' drop an image here, or click to select one</p>
      <p>Upload Either jpeg or png</p>
    </div>
  </div>
  {/* Other components */}
</div>
        <div
          className="modal fade"
          id="staticBackdrop"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabIndex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-4 fw-bold" id="staticBackdropLabel">
                  Emotion Recognition <i className="fab fa-aws"></i>
                </h1>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                {details ? (
                  <table className="table table-hover">
                    <tbody>
                      <tr>
                        <td>
                          <strong>Emotion:</strong> {details.emotions.type} <br />
                          <strong>Confidence:</strong> {details.emotions.confidence}{" "}
                          <br />
                          <strong>Glasses:</strong>{" "}
                          {details.glasses === true ? "Glasses are Present" : "No glasses"}{" "}
                          <br />
                          <strong>Sunglasses:</strong>{" "}
                          {details.sunglasses === true ? "SunGlasses are Present" : "No Sunglasses"}{" "}
                          <br />
                          <strong>Gender:</strong> {details.gender} <br />
                          <strong>Smile:</strong>{" "}
                          {details.smile === true ? "Smiling" : "Not Smiling"} <br />
                          <strong>Age Range:</strong> {details.ageRange.low} -{" "}
                          {details.ageRange.high}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                ) : (
                  <Loader />
                )}
                <div>
                  <h2 className="modal-subtitle">Emotion-Based Song Suggestions</h2>
                  <ul className="song-list">
                    {songRecommendations.map((song, index) => (
                      <li className="song-item" key={index}>
                        <div className="song-details">
                          <p className="song-name">{song.data.albumOfTrack.name}</p>
                          <a
                            href={song.data.albumOfTrack.sharingInfo.shareUrl}
                            className="spotify-link"
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            Listen on Spotify
                          </a>
                        </div>
                        <img
                          src={song.data.albumOfTrack.coverArt.sources[0].url}
                          alt="Song Icon"
                          className="song-icon"
                        />
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className="modal-footer">
              <button type="button" className="spotify-btn animate" data-bs-dismiss="modal">
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <Footer/>
      </div>
    </CloudinaryContext>
  );
}