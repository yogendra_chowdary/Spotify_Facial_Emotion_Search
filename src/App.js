
import React from 'react'
import ImageUpload from './Components/ImageUpload'
import "./App.css"
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

export default function App() {
  return (
    <>
  <ImageUpload></ImageUpload>
  <ToastContainer></ToastContainer>
  </>
  )
}
